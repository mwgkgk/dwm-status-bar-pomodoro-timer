#!/usr/bin/python2

import sys
import os
import time
from daemon import runner

class App():

    def __init__(self):
        self.stdin_path = '/dev/null'
        if sys.stdout.isatty():
            self.stdout_path = '/dev/tty'
            self.stderr_path = '/dev/tty'
        else:
            self.stdout_path = '/dev/null'
            self.stderr_path = '/dev/null'
            
        self.pidfile_path =  '/tmp/pomotimer.pid'
        self.pidfile_timeout = 5
        self.dot_file_path = os.path.expanduser('~/.pomotimer')
        self.timer_period = 25*60

    def run(self):
        start = time.time()
        try:
            while True:
                time.sleep(1)
                if (time.time() - start) < self.timer_period:
                    self.write_remaining_time(self.dot_file_path, time.time() - start)
                else:
                    break
        except Exception as e:
            print >> sys.stderr, e
        finally:
            try:
                os.remove(self.dot_file_path)
            except Exception as e:
                print >> sys.stderr, e
            sys.exit(0)

    def write_remaining_time(self, filename, rem_time):
        rem_time = int(self.timer_period - rem_time)
        minutes = rem_time / 60
        seconds = rem_time - minutes * 60
        with open(filename, 'w') as f:
            f.write(str(minutes)+":"+str(seconds).zfill(2))


app = App()
daemon_runner = runner.DaemonRunner(app)
daemon_runner.do_action()
